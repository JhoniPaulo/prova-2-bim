﻿using ProvaDiogo.Models;
using System.Collections.Generic;

namespace ProvaDiogo.Repositories
{
    public class FolhaRepositoryReceive
    {
        public static List<FolhaPagamento> folhasDePagamentoReceive = new List<FolhaPagamento>();

        public static FolhaPagamento Cadastrar(FolhaPagamento folha)
        {
            folhasDePagamentoReceive.Add(folha);
            return folha;
        }

        public static List<FolhaPagamento> Listar() => folhasDePagamentoReceive;
    }
}
