﻿using ProvaDiogo.Models;
using System.Collections.Generic;

namespace ProvaDiogo.Repositories
{
    public class FolhaRepository
    {
        public static List<FolhaPagamento> folhasDePagamento = folhasDePagamento = new List<FolhaPagamento>();

        public static FolhaPagamento Cadastrar(FolhaPagamento folha)
        {
            folhasDePagamento.Add(folha);
            return folha;
        }

        public static List<FolhaPagamento> Listar() => folhasDePagamento;
    }
}
