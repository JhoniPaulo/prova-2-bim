﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProvaDiogo.Models;
using ProvaDiogo.Repositories;
using RabbitMQ.Client;
using System.Collections.Generic;
using System.Text;

namespace ProvaDiogo.Controllers
{
    [Route("folha")]
    [ApiController]
    public class FolhaController : ControllerBase
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private const string FILA = "mensagens";
        public FolhaController()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                HostName = "localhost"
            };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: FILA,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
        }

        [Route("cadastrar")]
        [HttpPost]
        public IActionResult Cadastrar([FromBody] FolhaPagamento folha)
        {

            FolhaRepository.Cadastrar(folha);

            List<FolhaPagamento> folhas = new List<FolhaPagamento>();
            folhas = FolhaRepository.Listar();

            foreach (FolhaPagamento item in folhas)
            {
                item.bruto = item.horas * item.valor;
                item.irrf = calcularIRRF(item.bruto);
                item.inss = calcularINSS(item.bruto);
                item.fgts = calcularFGTS(item.bruto);
                item.liquido = calcularSalarioLiquido(item.bruto, item.irrf, item.inss);
            }

            string mensagem = JsonConvert.SerializeObject(folhas);
            byte[] bytes = Encoding.UTF8.GetBytes(mensagem);

            _channel.BasicPublish(
                body: bytes,
                routingKey: "mensagens",
                basicProperties: null,
                exchange: ""
            );
            return Ok();
        }


        [Route("listar")]
        [HttpGet]
        public IActionResult Listar([FromBody] FolhaPagamento folha)
        {
            List<FolhaPagamento> folhas = new List<FolhaPagamento>();
            folhas = FolhaRepositoryReceive.Listar();
            return Ok(folhas);
        }

        [Route("total")]
        [HttpGet]
        public double Total([FromBody] FolhaPagamento folha)
        {
            List<FolhaPagamento> folhas = new List<FolhaPagamento>();
            folhas = FolhaRepositoryReceive.Listar();

            double valor = 0;

            foreach (var item in folhas)
            {
                valor = valor + item.bruto;
            }

            return valor;
        }

        [Route("media")]
        [HttpGet]
        public IActionResult Media([FromBody] FolhaPagamento folha)
        {
            List<FolhaPagamento> folhas = new List<FolhaPagamento>();
            folhas = FolhaRepositoryReceive.Listar();

            double valor = 0;

            foreach (var item in folhas)
            {
                valor = valor + item.bruto;
            }

            return Ok(new
            {
                valorTotal = valor,
                quantidadeFolhas = folhas.Count,
                media = valor / folhas.Count
            });
        }

        public double calcularIRRF(double bruto)
        {
            if (bruto <= 1903.98)
            {
                return 0;
            }
            else if (bruto <= 2826.65)
            {
                return bruto * 0.075 - 142.8;
            }
            else if (bruto <= 3751.05)
            {
                return bruto * 0.15 - 354.8;
            }
            else if (bruto <= 4664.68)
            {
                return bruto * 0.225 - 636.13;
            }
            return bruto * 0.275 - 869.39;
        }

        public double calcularINSS(double bruto)
        {
            if (bruto <= 1693.72)
            {
                return bruto * 0.08;
            }
            else if (bruto <= 2822.9)
            {
                return bruto * 0.09;
            }
            else if (bruto <= 5645.8)
            {
                return bruto * 0.11;
            }
            return 621.03;
        }

        public double calcularFGTS(double bruto)
        {
            return bruto * 0.08;
        }

        public double calcularSalarioLiquido(double bruto, double irrf, double inss)
        {
            return bruto - irrf - inss;
        }
    }
}
