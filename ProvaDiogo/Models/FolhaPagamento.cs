﻿namespace ProvaDiogo.Models
{
    public class FolhaPagamento
    {
        public double mes { get; set; }
        public double ano { get; set; }
        public double horas { get; set; }
        public double valor { get; set; }
        public double bruto { get; set; }
        public double irrf { get; set; }
        public double inss { get; set; }
        public double fgts { get; set; }
        public double liquido { get; set; }
        public Funcionario funcionario { get; set; }
    }
}
